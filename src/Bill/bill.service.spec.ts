import { Test, TestingModule } from '@nestjs/testing';
import { BillesService } from './Bill.service';

describe('BillService', () => {
  let service: BillesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BillesService],
    }).compile();

    service = module.get<BillesService>(BillesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
