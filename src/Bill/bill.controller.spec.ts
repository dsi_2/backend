import { Test, TestingModule } from '@nestjs/testing';
import { BillController } from './Bill.controller';
import { BillesService } from './Bill.service';

describe('BillController', () => {
  let controller: BillController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BillController],
      providers: [BillesService],
    }).compile();

    controller = module.get<BillController>(BillController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
