import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BillesService } from './Bill.service';
import { CreateBillDto } from './dto/create-Bill.dto';
import { UpdateBillDto } from './dto/update-Bill.dto';

@Controller('Bill')
export class BillController {
  constructor(private readonly BillService: BillesService) {}

  @Post()
  create(@Body() createBillDto: CreateBillDto) {
    return this.BillService.create(createBillDto);
  }

  @Get()
  findAll() {
    return this.BillService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.BillService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBillDto: UpdateBillDto) {
    return this.BillService.update(+id, updateBillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.BillService.remove(+id);
  }
}
