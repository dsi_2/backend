import { Module } from '@nestjs/common';
import { BillesService } from './Bill.service';
import { BillController } from './Bill.controller';
import { Bill } from './entities/Bill.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Bill])],
  controllers: [BillController],
  providers: [BillesService],
})
export class BillModule {}
