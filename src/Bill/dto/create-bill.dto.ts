import { IsNotEmpty } from 'class-validator';

export class CreateBillDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  salary: number;

  @IsNotEmpty()
  position: string;

  @IsNotEmpty()
  worktime: number;
}
