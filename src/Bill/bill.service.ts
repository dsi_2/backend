import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDto } from './dto/create-Bill.dto';
import { UpdateBillDto } from './dto/update-Bill.dto';
import { Bill } from './entities/Bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BillesService {
  constructor(
    @InjectRepository(Bill)
    private BillesRepository: Repository<Bill>,
  ) {}

  create(createBillDto: CreateBillDto): Promise<Bill> {
    return this.BillesRepository.save(createBillDto);
  }

  findAll(): Promise<Bill[]> {
    return this.BillesRepository.find();
  }

  findOne(id: number) {
    return this.BillesRepository.findOneBy({ id: id });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    await this.BillesRepository.update(id, updateBillDto);
    const Bill = await this.BillesRepository.findOneBy({ id });
    return Bill;
  }

  async remove(id: number) {
    const deleteBill = await this.BillesRepository.findOneBy({ id });
    return this.BillesRepository.remove(deleteBill);
  }
}
