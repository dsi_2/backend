import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { User } from './users/entities/user.entity';
import { Bill } from './Bill/entities/bill.entity';
import { BillModule } from './Bill/bill.module';
import { MemberModule } from './member/member.module';
import { Member } from './member/entities/member.entity';
import { Role } from './roles/entities/role.entity';
import { Order } from './orders/entities/order.entity';
import { OrdersModule } from './orders/orders.module';
import { OrderItem } from './orders/entities/orderItem.entity';
import { AuthModule } from './auth/auth.module';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { Type } from './types/entities/type.entity';
import { TypesModule } from './types/types.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { EmployeeModule } from './employee/employee.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      entities: [Bill, User, Member, Role, Order, OrderItem, Product, Type],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    TemperatureModule,
    UsersModule,
    BillModule,
    MemberModule,
    OrdersModule,
    AuthModule,
    ProductsModule,
    TypesModule,
    EmployeeModule,
  ],

  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
