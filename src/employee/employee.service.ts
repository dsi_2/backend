import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeeService {
  lastId: number = 1;
  employee: Employee[] = [
    {
      id: 1,
      name: 'phongphak',
      email: 'phongphak@mail.com',
      password: 'Pass@1234',
      roles: ['employee'],
      gender: 'female',
      salaryType: 'Part-Time',
      salary: 12000,
      phone: '082XXXXXX',
      wagePerHour: 200,
      branch_id: [2],
    },
  ];
  create(createEmployeeDto: CreateEmployeeDto) {
    this.lastId++;
    const newEmployee = { ...createEmployeeDto, id: this.lastId };
    this.employee.push(newEmployee);
    return newEmployee;
  }

  findAll() {
    return this.employee;
  }

  findOne(id: number) {
    const index = this.employee.findIndex((employee) => employee.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    return this.employee[index];
  }

  update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const index = this.employee.findIndex((employee) => employee.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    this.employee[index] = { ...this.employee[index], ...updateEmployeeDto };
    return this.employee[index];
  }

  remove(id: number) {
    const index = this.employee.findIndex((employee) => employee.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    this.employee.splice(index, 1);
    return this.employee[index];
  }
}
