import { IsNotEmpty, Length } from 'class-validator';

export class CreateMemberDto {
  @IsNotEmpty()
  @Length(4, 32)
  Fullname: string;
  @IsNotEmpty()
  Tel: string;
  Point: number;
}
