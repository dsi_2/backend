import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private membersRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    return this.membersRepository.save(createMemberDto);
  }

  findAll(): Promise<Member[]> {
    return this.membersRepository.find();
  }

  findOne(id: number) {
    return this.membersRepository.findOneBy({ id });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.membersRepository.update(id, updateMemberDto);
    const member = await this.membersRepository.findOneBy({ id });
    return member;
  }

  async remove(id: number) {
    const deleteMember = await this.membersRepository.findOneBy({ id });
    return this.membersRepository.remove(deleteMember);
  }
}
