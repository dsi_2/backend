import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  Fullname: string;
  @Column()
  Tel: string;
  @Column()
  Point: number;
}
